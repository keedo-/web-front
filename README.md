# Keedo Frontend

## Summary

This is the frontend web application of [Keedo](https://keedo.tn) is a virtual class management. 

## Features

* TODO

## Status

* In progress

## Requirements
 
platform : Nodejs & Vue js 

## Dependencies 

backend application :  [backend app](  https://gitlab.com/keedo-/backend)

## License

MIT
