import base from "./base";

export default {
	login(data = null) {
		return base.request('post', 'accounts/login', data)
	},
	register(data = null) {
		return base.request('post', 'accounts/sign-up', data)
	},
	logout() {
		return base.request('post', 'accounts/logout')
	},
	forgotPassword(data = null) {
		return base.request('post', 'accounts/forgot-password', data)
	},
	resetPassword(data = null) {
		return base.request('post', 'accounts/reset-password', data)
	},
	requestSchool(data) {
		return base.request('post', 'teacher/profile/request-school-profile', data)
	},
	requestTeacherProfile(data) {
		return base.request('post', 'teacher/profile/request-teacher-profile', data)
	},
	invite(data) {
		return base.request('post', 'accounts/invite', data)
	}
}
