import base from "./../base";

export default {
	list() {
		return base.request('get', 'admin/accounts')
	},
	create(data, options = null) {
		return base.request('post', 'admin/accounts', data, options)
	}
}
