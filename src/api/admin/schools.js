import base from "./../base";

export default {
	list() {
		return base.request('get', 'admin/schools')
	},
	create(data, options = null) {
		return base.request('post', 'admin/schools', data, options)
	}
}
