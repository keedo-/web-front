import axios from 'axios';

let baseApi = process.env.baseApi || 'http://192.168.1.29:8001/api/';

// Add a 401 response interceptor
axios.interceptors.response.use(function (response) {
	return response;
}, function (error) {
	if (401 === error.response.status) {
		// here block it
		window.location = `/accounts/login/?redirect=${window.location.pathname}`
	} else {
		return Promise.reject(error);
	}
});
export default {
	getBaseApi() {
		return baseApi;
	},
	request(method, uri, data = null, options = null) {
		if (!method) {
			return
		}

		if (!uri) {
			return
		}
		options = options || {};

		let url = baseApi + uri;
		let config = {method: method, url, data, withCredentials: true};
		if (options.headers) {
			config['headers'] = options.headers;
		}
		return axios(config);
	}
}
