import base from "./base";

export default {
    list(options) {
        return base.request('get', 'brands')
    },
    products(id, options) {
        return base.request('get', `brands/${id}/products`)
    },
    followers(id, options) {
        return base.request('get', `brands/${id}/followers`)
    },
    follow(id, options) {
        return base.request('post', `brands/${id}/follow`)
    },
    unFollow(id, options) {
        return base.request('post', `brands/${id}/unfollow`)
    }
}