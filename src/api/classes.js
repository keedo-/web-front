import base from "./base";

export default {
	list() {
		return base.request('get', 'classes/')
	},
	info(id) {
		return base.request('get', `classes/${id}`);
	}
}
