import base from "./base";

export default {
	info() {
		return base.request('get', 'profile');
	},
	update(data, options = null) {
		return base.request('post', 'profile', data, options)
	}
}
