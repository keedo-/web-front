import base from "./base";

let api = {
	data: null,
	list() {
		return new Promise(function (resolve, reject) {
			if (!api.data) {
				base.request('get', 'schools').then(response => {
					api.data = response.data;
					resolve(api.data);
				}).catch(reject)
			} else {
				resolve(api.data);
			}
		});
	}
};
export default api;
