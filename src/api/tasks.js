import base from "./base";

export default {
	list() {
		return base.request('get', 'tasks/')
	}
}
