import base from "../base";

export default {
	list(idClass) {
		return base.request('get', ['teacher', 'classes', idClass, 'chapters'].join('/'))
	},
	info(idClass, idChapter) {
		return base.request('get', ['teacher', 'classes', idChapter, 'chapters', idChapter].join('/'))
	},
	create(data, options = {}) {
		options.headers = {
			'Content-Type': 'application/json'
		};
		return base.request('post', 'teacher/classes', data, options)
	},
	getPictureUpload(id) {
		return base.getBaseApi() + 'teacher/classes/' + id + '/pictures/upload/';
	},
	getPictures(id) {
		return base.request('get', 'teacher/classes/' + id + '/pictures/');
	},
}
