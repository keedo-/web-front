import base from "../base";

export default {
	info(id = null) {
		return base.request('get', 'teacher/classes/' + (id || ''))
	},
	invite(id, data) {
		return base.request('post', ['teacher/classes', id, 'students', 'invite'].join('/'), data);
	},
	list() {
		return base.request('get', 'teacher/classes/')
	},
	create(data, options = {}) {
		options.headers = {
			'Content-Type': 'application/json'
		};
		return base.request('post', 'teacher/classes', data, options)
	},
	update(data, options = {}) {
		options.headers = {
			'Content-Type': 'application/json'
		};
		return base.request('put', 'teacher/classes', data, options)
	}
}
