import base from "../base";

export default {
	info() {
		return base.request('get', 'business/school')
	},
	mySchools() {
		return base.request('get', 'teacher/schools/mine')
	},
	create(data, options = null) {
		return base.request('post', 'business/school', data, options)
	}
}
