import Loader from './Loader.vue'


var LoaderPlugin = {
	install(Vue) {
		Vue.component('app-loader', Loader)
	}
};

export default LoaderPlugin
