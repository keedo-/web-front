import Logo from './Logo.vue'


var LogoPlugin = {
	install(Vue) {
		Vue.component('app-logo', Logo)
	}
};

export default LogoPlugin
