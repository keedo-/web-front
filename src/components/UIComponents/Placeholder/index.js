import Placeholder from './Placeholder.vue'


var AppPlaceholderPlugin = {
	install(Vue) {
		Vue.component('appPlaceholder', Placeholder)
	}
};
export default AppPlaceholderPlugin
