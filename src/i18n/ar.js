export default {
	"common.home": "الرئيسية",
	"common.soon": "قريبا",
	"common.update": "تحديث",
	"common.save": "حفظ",
	"common.create": "حفظ",
	"common.confirm": "حفظ",
	"common.list_empty": "القائمة فارغة",
	"common.notifications": "الإشعارات",
	"common.request.confirm": " تأكيد الطلب",
	"common.request.success": "لقد تم تسجيل طلبك، سيقع الاتصال بك قريبا",
	"common.date_notice": "نضرا لبعض المشاكل الفنية،سيكون التطبيق متاحًا في 30 أفريل .هناك بعض الميزات قيد التطوير ، يرجى الاتصال بنا إذا حدث أي عطل.",
	"common.covid_notice_1": "  المنصة مجانية خلال فترةالحجر (covid-19).",
	"common.covid_notice_2": "لننشأ لك حسابا ",
	"common.covid_notice_contact": "اتصل بنا",
	"common.notfound_or_under_construction": "صفحة  غير موجودة  أو قيد الإنشاء ",


	"login.email": "البريد الإلكتروني",
	"pricing.contact": "اختر العرض",

	"welcome.hero.title": "كن مع تلامذتك في بيوتهم كما بالقسم",
	"welcome.hero.subtitle": "  إدارة الصف على الإنترنت بكل سهولة",

	"common.createdAt": "تاريخ الإنشاء",
	"common.updatedAt": "تاريخ التعديل",

	"welcome.heros.title": "الميزات الضرورية",
	"welcome.heros.subtitle": "لتبسيط إنشاء وإدارة الصف عبر الإنترنت ",
	"welcome.heros.connection.title": "تواصل مع تلامذتك",
	"welcome.heros.connection.subtitle": ". تواصل مع تلامذتك بكل أمان متى وكيف أردت . خطط لتمارينك و انشأ دروسك بسهولة.",

	"welcome.heros.creativity.title": "ليكن الابداع متاحا و سهلا",
	"welcome.heros.creativity.subtitle": "أطلق العنان لخيال وإبداع تلامذتك",


	"header.features": "الخصائص",
	"header.demo": "عرض",


	"register.title": "حساب جديد",

	"login.title": "دخول",
	"login.password": "كلمة السر",
	"login.register": "حساب جديد",
	"login.login": "دخول",
	"login.username": "البريد الإلكتروني",
	"forgotPassword.resend": "اعادة الارسال",
	"forgotPassword.emailSent": "لقد تم ارسال الرابط لبريدك الالكرتوني",
	"login.forgetPassword": "نسيت كلمة السر",
	"forgotPassword.email": "البريد الإلكتروني",
	"forgotPassword.title": "اعادة الارسال",
	"forgotPassword.sendMeMail": "أرسل لي رابط إستعادة كلمة المرور",
	"resetPassword.title": "تجديد كملة المرور",
	"resetPassword.password": "كلمة المرور الجديدة",
	"resetPassword.confirmPassword": " تأكيد كلمة المرور",
	"resetPassword.passwordRestSuccess": "تم اعادة ادخال كلمة السر",

	"register.lastName": "الإسم",
	"register.firstName": "اللقب",
	"register.login": "دخول",
	"register.email": "البريد الإلكتروني",
	"register.re_email": "تأكيد البريد الإلكتروني",
	"register.password": "تأكيد كلمة السر",
	"register.rePassword": "كلمة السر",
	"register.accountCreated": "تم إنشاء الحساب بنجاح ، يرجى التحقق من بريدك الإلكتروني لتأكيد حسابك",

	"pricing.unit": "dt",
	"pricing.public_school": "تعليم عام",
	"pricing.private_school": "مدرسة خاصة",
	"pricing.public_school_cost": "3",
	"pricing.private_school_cost": "10",


	"footer.links.product": "المنتج",
	"footer.links.class_management": "إدارة الفصول الدراسية",
	"footer.links.planning": "التخطيط",
	"footer.links.library": "المكتبة",
	"footer.links.activities": " الأنشطة",

	"footer.links.company": "Keedo",
	"footer.links.blog": "المدونة",
	"footer.links.legal": "الإشعارات القانونية",
	"footer.links.faq": "الأسئلة المتداولة",
	"footer.links.contact": " اتصل بنا",
	"footer.links.support": "الدعم",

	"footer.links.social": "تواصل معنا",
	"footer.links.gitlab": "Gitlab",
	"footer.links.business": "Affaires",
	"footer.links.shops": "Boutiques",


	"pricing.choose_plan": "اختر أحد عروضنا",
	"pricing.our_plans": "عروضنا",
	"pricing.features_list": "دروس، تخطيط، تمارين، دردشة خاصة، مكالمة جماعية",
	"pricing.user_month": "تلميذ / الشهر",
	"pricing.on_promise_hosted_on_you": "مثبتة على الخادم الخاص بك",
	"pricing.on_promise_plugin": "تطوير حسب الطلب وإدماج برامج أخرى",
	"pricing.customisation": "تخصيص و تغيير خصائص حسب الطلب",
	// WELCOME FEATURES
	"features.section.title": "",
	"features.section.subtitle": "",
	"features.section.book": "",
	"features.section.bookDescription": "",
	"features.section.calendar": "",
	"features.section.calendarDescription": "",
	"features.section.activity": "",
	"features.section.activityDescription": "",
	"features.section.class": "",
	"features.section.classDescription": "",
	"features.section.chat": "",
	"features.section.chatDescription": "",
	"features.section.library": "",
	"features.section.libraryDescription": "",
	// TEACHER
	"teacher.Dashboard.SideBar.Dashboard": 'الرئيسية',
	"teacher.Dashboard.SideBar.Classes": 'الأقسام',
	"teacher.Dashboard.SideBar.Resources": 'الموارد',
	"teacher.Dashboard.SideBar.Chat": 'المحادثات',
	"teacher.Dashboard.SideBar.Planning": 'جدول الدروس',
	"teacher.Dashboard.SideBar.Settings": 'الإعدادات',

	"dashboard.classes.list.create.title": "قسم جديد",
	"dashboard.classes.list.create.name": "إسم القسم",
	"dashboard.classes.list.create.description": "وصف القسم",
	"dashboard.classes.list.create.school": "المدرسة",

	"teacher.classes.list_title": "الأقسام",


	"student.Dashboard.SideBar.Dashboard": 'الرئيسية',
	"student.Dashboard.SideBar.Classes": 'أقسامي',
	"student.Dashboard.SideBar.Chat": 'المحادثات',
	"student.Dashboard.SideBar.Profile": 'معلوماتي',
	"student.Dashboard.SideBar.Planning": 'جدول الدروس',
	"student.Dashboard.SideBar.Settings": 'حسابي',

	"student.Classes.list_title": "الأقسام",
	"student.overview.tasks": "التمارين",

	"student.profile.avatar": "الصورة",
	"student.profile.title": "معلوماتي",

	"admin.dashboard.sidebar.overview": "Overview",
	"admin.dashboard.sidebar.schools": "Ecoles",
	"admin.dashboard.sidebar.accounts": "Comptes",
	"admin.dashboard.sidebar.schools_requests": "Schools Requests",
	"admin.dashboard.sidebar.teachers_requests": "Teachers requests",
	"admin.dashboard.sidebar.settings": "Paramètres",


	"admin.schools_requests.list.title": "طلبات المدارس",
	"admin.teachers_requests.list.title": "طلبات المعلمين",
	"admin.accounts.list.title": "الحسابات",

	'admin.accounts.list.table.header.email': 'البريد الإلكتروني',
	'admin.accounts.list.table.header.role': 'role',

	"admin.Schools.list.table.header.name": "Nom",
	"admin.Schools.list.table.header.address": "Addresse",
	"admin.Schools.list.table.header.description": "Déscription",
	"admin.Schools.list.table.header.created_at": "Créeé Le",
	"admin.Schools.list.table.header.updated_at": "Dernière mise à jour",
	"admin.Schools.list.create.create_new": "Créer une nouvelle école",
	"admin.Schools.list.create.name": "اسم المدرسة",
	"admin.Schools.list.create.description": "Description",
	"admin.Schools.list.create.address": "Addresse",
	"admin.Schools.list.create.addressLocation": "Position géo ",
	"admin.Schools.list.create.addressLat": "Latitude",
	"admin.Schools.list.create.addresslng": "Longitude",

	"server.error": "خطأ بالخادم",


	"settings.advanced": "اعدادت متقدمة",
	"settings.account": "حساب",
	"settings.logout": "تسجيل الخروج",
	"header.am_i_teacher?": "هل انت مدرس ?",


	"accounts.request.select_school": "إختر مدرسة",
	"accounts.request_teacher_profile": "طلب حساب معلم",
	"accounts.school_not_found": "لم أجد المدرسة ، طلب ملف المدرسة",


	"teacher.dashboard.classes.list.table.header.name": "إسم القسم",
}
