// Ready translated locale messages
import fr from './fr'
import en from './en'
import ar from './ar'
export default {
    en: en,
    fr: fr,
    ar : ar
}
