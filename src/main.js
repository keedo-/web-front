import Vue from 'vue'
import 'es6-promise/auto'
import VueRouter from 'vue-router'
import VueI18n from 'vue-i18n'
import messages from "./i18n"

import './sass/style.scss';

const $ = window.$;
// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import Notifications from './components/UIComponents/NotificationPlugin'
import LoaderPlugin from './components/UIComponents/Loader'
import Logo from './components/UIComponents/Logo'
import Placeholder from "./components/UIComponents/Placeholder";

import App from './App.vue';
// router setup
import routes from './routes/routes'

// library imports
import Chartist from 'chartist'

import {sync} from 'vuex-router-sync'

import store from './store'

Vue.use(VueI18n);
Vue.use(VueRouter);
// plugin setup
Vue.use(GlobalComponents);
Vue.use(GlobalDirectives);
Vue.use(Notifications);
Vue.use(LoaderPlugin);
Vue.use(Logo);
Vue.use(Placeholder);

// ignore ion-icons components
Vue.config.ignoredElements = [/^ion-/]

// Create VueI18n instance with options
const i18n = new VueI18n({
	// see Footer.vue
	locale: localStorage.getItem('LOCALE') || 'fr', // set locale
	messages, // set locale messages
});
// configure router
const router = new VueRouter({
	routes, // short for routes: routes
	linkActiveClass: 'active',
	mode: 'history',
	scrollBehavior: function (to, from, savedPosition) {
		return savedPosition || {x: 0, y: 0}
	}
});
// Some middleware to help us ensure the user is authenticated.
router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth) && (!router.app.$store.state.user === 'null')) {
		// this route requires auth, check if logged in
		// if not, redirect to login page.
		window.console.log('Not authenticated')
		next({
			path: '/accounts/login',
			query: {redirect: to.fullPath}
		})
	} else {
		next()
	}
});

/* Some middleware to help us ensure the user is a teacher .
router.beforeEach((to, from, next) => {
	let user = router.app.$store.state.user;
	if (to.matched.some(record => record.meta.requireIsTeacher) && (user.accountType !== "TEACHER")) {
		// this route requires auth, check if logged in
		// if not, redirect to login page.
		window.console.log('Not Teacher');
		next({
			path: '/dashboard/overview'
		})
	} else {
		next()
	}
});

*/

sync(store, router);
// Check local storage to handle refreshes
if (window.localStorage) {
	let localUserString = window.localStorage.getItem('user') || 'null';
	let localUser = JSON.parse(localUserString);

	if (localUser && store.state.user !== localUser) {
		store.commit('SET_USER', localUser);
	}
}
// global library setup
Object.defineProperty(Vue.prototype, '$Chartist', {
	get() {
		return this.$root.Chartist
	}
});

/* eslint-disable no-new */
new Vue({
	el: '#app',
	render: h => h(App),
	router,
	store,
	i18n,
	data: {
		Chartist: Chartist
	}
});
// play animation
$(document).ready(function () {
	initScrollReveal();
});

function initScrollReveal() {
	window.sr = window.ScrollReveal(); // Simple reveal
	let sr = window.sr;
	sr.reveal('.is-title-reveal', {
		origin: 'bottom',
		distance: '20px',
		duration: 600,
		delay: 100,
		rotate: {
			x: 0,
			y: 0,
			z: 0
		},
		opacity: 0,
		scale: 1,
		easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
		container: window.document.documentElement,
		mobile: true,
		reset: false,
		useDelay: 'always',
		viewFactor: 0.2
	}); // Revealing features

	sr.reveal('.is-feature-reveal', {
		origin: 'bottom',
		distance: '20px',
		duration: 600,
		delay: 100,
		rotate: {
			x: 0,
			y: 0,
			z: 0
		},
		opacity: 0,
		scale: 1,
		easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
		container: window.document.documentElement,
		mobile: true,
		reset: true,
		useDelay: 'always',
		viewFactor: 0.2
	}, 160); // Revealing features
	sr.reveal('.is-icon-reveal', {
		origin: 'bottom',
		distance: '20px',
		duration: 600,
		delay: 100,
		rotate: {
			x: 0,
			y: 0,
			z: 0
		},
		opacity: 0,
		scale: 1,
		easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
		container: window.document.documentElement,
		mobile: true,
		reset: false,
		useDelay: 'always',
		viewFactor: 0.2
	}, 160);
}
