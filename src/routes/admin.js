// App
import AppLayout from '../components/AdminDashboard/Layout/DashboardLayout' ;
import DefaultLayout from "../components/CommonViews/DefaultLayout";
import Overview from '../components/AdminDashboard/Views/Overview' ;


import SchoolsList from '../components/AdminDashboard/Views/Schools/List';
import AccountsList from '../components/AdminDashboard/Views/Accounts/List';
import TeacherRequestsList from "../components/AdminDashboard/Views/Teachers/TeacherRequestsList";
import SchoolsRequestsList from "../components/AdminDashboard/Views/Schools/SchoolsRequestsList";


export default {
	path: '/admin',
	component: AppLayout,
	redirect: '/admin/',
	name: 'AdminRoute',
	meta: {description: 'Admin route', requiresAuth: true, requireIsAdmin: true},
	children: [
		{
			path: '/',
			name: 'overview',
			component: Overview
		},
		{
			path: 'schools-requests',
			name: 'SchoolsRequestsList',
			component: SchoolsRequestsList
		},
		{
			path: 'teachers-requests',
			name: 'TeacherRequestsList',
			component: TeacherRequestsList
		},
		{
			path: 'accounts',
			name: 'accountsRoute',
			component: AccountsList
		},
		{
			path: 'schools',
			component: DefaultLayout,
			redirect: '/admin/schools',
			name: 'adminSchoolsRoute',
			children: [
				{
					path: '',
					name: 'schoolsList',
					component: SchoolsList
				}
			]
		},
	]
};
