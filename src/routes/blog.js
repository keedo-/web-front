// App
import BlogLayout from '../components/Blog/Layout/Base' ;

import Overview from '../components/Blog/Views/Overview' ;
import Welcome from '../components/Blog/Views/Welcome' ;


export default {
	path: '/blog',
	component: BlogLayout,
	redirect: '/blog/',
	name: 'BlogRoute',
	meta: {description: 'Blog route'},
	children: [
		{
			path: '/',
			name: 'overview',
			component: Overview
		},
		{
			path: 'hello',
			name: 'Welcome',
			component: Welcome
		}
	]
};
