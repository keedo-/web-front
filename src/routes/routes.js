// GeneralViews
import NotFound from '../components/CommonViews/NotFoundPage.vue'
import TosView from '../components/CommonViews/tos.vue'


import LoginView from '../components/Accounts/Login.vue'
import RegisterView from '../components/Accounts/Register.vue'
import ForgetPassword from '../components/Accounts/ForgetPassword.vue'
import ResetPassword from "../components/Accounts/ResetPassword";
import RequestSchool from "../components/Accounts/RequestSchool";
import RequestTeacher from "../components/Accounts/RequestTeacher";

import Welcome from '../components/Welcome.vue'


import TeacherRoutes from './teacher'
import AdminRoutes from './admin'
import BlogRoutes from './blog'
import StudentRoutes from './student'

const routes = [
			{
				path: '/',
				component: Welcome
			},
			{
				path: '/accounts/login',
				component: LoginView
			},
			{
				path: '/accounts/forgot-password',
				component: ForgetPassword
			},
			{
				path: '/accounts/reset-password/:token',
				component: ResetPassword
			},
			{
				path: '/accounts/register',
				component: RegisterView
			},
			{
				path: '/accounts/request-school',
				component: RequestSchool
			},
			{
				path: '/accounts/request-teacher-profile',
				component: RequestTeacher
			},
			{
				path: '/tos',
				component: TosView
			},
			BlogRoutes,
			AdminRoutes,
			StudentRoutes,
			TeacherRoutes,
			{
				path: '*', component:
				NotFound
			}
		]
;

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
