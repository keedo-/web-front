import DashboardLayout from '../components/SDashboard/Layout/Layout.vue'


import Overview from '../components/SDashboard/Views/Overview.vue'

import ClassesList from '../components/SDashboard/Views/Classes/List.vue'
import ClassesView from '../components/SDashboard/Views/Classes/View.vue'

import Planning from '../components/SDashboard/Views/Planning/List.vue'
import ProfileEdit from '../components/SDashboard/Views/ProfileEdit.vue'
import Settings from '../components/SDashboard/Views/Settings/Settings.vue'
import Chat from '../components/SDashboard/Views/Chat/Chat'


const routes = {
			path: '/dashboard',
			meta: {description: 'List of our servers', requiresAuth: true},
			component: DashboardLayout,
			redirect: '/dashboard/overview',
			children: [
				{
					path: 'overview',
					component: Overview

				},
				{
					path: 'classes',
					component: ClassesList

				},
				{
					path: 'classes/:id',
					component: ClassesView

				},
				{
					path: 'chat',
					component: Chat
				},
				{
					path: 'Planning',
					component: Planning

				},
				{
					path: 'student/profile',
					component: ProfileEdit

				},
				{
					path: 'student/settings',
					component: Settings

				}
			]
		}
;

export default routes
