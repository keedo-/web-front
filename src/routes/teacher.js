// App
import AppLayout from '../components/TDashboard/Layout/DashboardLayout'
import Overview from '../components/TDashboard/Views/Overview'


import ClassesList from '../components/TDashboard/Views/Classes/List'
import ClassView from '../components/TDashboard/Views/Classes/View'


import Resources from '../components/TDashboard/Views/Resources/List'

import Planning from '../components/TDashboard/Views/Planning/List'

import Chat from '../components/TDashboard/Views/Chat/List'


import DefaultLayout from "../components/CommonViews/DefaultLayout";
import Settings from "../components/TDashboard/Views/Settings/Settings";


export default {
	path: '/dashboard/t',
	component: AppLayout,
	redirect: '/dashboard/t/',
	name: 'TeacherRoute',
	meta: {description: 'Teacher route', requiresAuth: true, requireIsTeacher: true},
	children: [
		{
			path: '/',
			name: 'overview',
			component: Overview
		},
		{
			path: 'classes',
			component: DefaultLayout,
			redirect: '/dashboard/t/classes',
			name: 'classesRoute',
			children: [
				{
					path: '',
					name: 'classes',
					component: ClassesList
				},
				{
					path: ':id',
					name: 'classesView',
					component: ClassView
				}
			]
		},
		{
			path: 'resources',
			name: 'resources',
			component: Resources
		},
		{
			path: 'planning',
			name: 'planning',
			component: Planning
		},
		{
			path: 'chat',
			name: 'chat',
			component: Chat
		},
		{
			path: 'settings',
			component: Settings

		}
	]
};
