const COMMIT_TYPES = {
	"SET_USER": "SET_USER",
	"TOGGLE_LOADING": "TOGGLE_LOADING",
	"SET_TOKEN": "SET_TOKEN",
	"UPDATE_PROFILE": "UPDATE_PROFILE",
	"PROFILE": "PROFILE"

};
export default COMMIT_TYPES;
