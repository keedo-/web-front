export default {
    callingAPI: false,
    searching: '',
    serverURI: 'http://localhost:8001',
    user: null,
    token: null,
    userInfo: {
        messages: [{1: 'test', 2: 'test'}],
        notifications: [],
        tasks: []
    }
}
